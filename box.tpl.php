<?php
  if (!empty($content)) : ?>
      <!-- Region: osmobi-region-box -->
      <div class="osmobi-region osmobi-region-box">
<?php if ( !empty($title) ) : ?>      
        <div class="osmobi-region-title"><?php print $title ?></div>
<?php endif;?>
        <div class="osmobi-region-content">
<?php print $content; ?>
        </div>
      </div>
      <!-- End Region: osmobi-region-box -->
<?php endif; ?>